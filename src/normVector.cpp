#include <Rcpp.h>
using namespace Rcpp;

// This is a simple example of exporting a C++ function to R. You can
// source this function into an R session using the Rcpp::sourceCpp 
// function (or via the Source button on the editor toolbar). Learn
// more about Rcpp at:
//
//   http://www.rcpp.org/
//   http://adv-r.had.co.nz/Rcpp.html
//   http://gallery.rcpp.org/
//

// [[Rcpp::export]]
NumericVector normVector(NumericVector x, bool percent = false) {
  int n = x.size();
  NumericVector result(n);
  
  if(percent){                        // calculate percent change from base
    for(int i = 0; i<n;i++) {
      result[i] = (x[i]-x[0])/x[0];
    } 
  } else {
    for(int i = 0; i<n;i++) {
      result[i] = x[i]/x[0];
    }
  }
  
  return result;
}


