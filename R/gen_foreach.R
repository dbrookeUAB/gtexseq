#' Preconstructed `foreach` function to parallize common applications
#'
#' @param dt  a data.frame object
#' @param int_list the list of objects used by the foreach loop
#' @param FUN  the function used in the foreach loop
#' @param PACKS packages to export into clusters
#' @param export a vector that includes environment objects to export into each cluster
#' @param final.names column names for the final result. Default is c('int_list','result')
#'
#' @return
#' @export
#' @import foreach
#' @importFrom parallel makeCluster detectCores stopCluster
#' @importFrom doSNOW registerDoSNOW
#' @importFrom progress progress_bar
#' @importFrom crayon style bold green red
#'  
#'
#' @examples
gen_foreach <- function(dt = NULL,
                        int_list,
                        FUN,
                        PACKS = NULL,
                        export = NULL,
                        final.names = c('int_list', 'result'),
                        return = TRUE,
                        combine = 'c') {
  # loading parallel and doSNOW package and creating cluster ----------------
  
  require(foreach, quietly = TRUE, warn.conflicts = FALSE)
  
  if (is.null(PACKS)) {
    PACKS <- c('data.table')
  } else {
    PACKS <- c('data.table', PACKS)
  }
  
  cat(crayon::red('creating cluster\n'))
  
  numCores <- parallel::detectCores()
  cl <- parallel::makeCluster(numCores)
  doSNOW::registerDoSNOW(cl)
  on.exit(parallel::stopCluster(cl))
  e <- simpleError("error occured")
  # progress bar ------------------------------------------------------------
  iterations <-
    length(int_list)                               # used for the foreach loop
  
  pb <- progress::progress_bar$new(
    format = ":percent item = :item [:bar] :elapsed | eta: :eta",
    total = iterations,
    width = floor(options()$width*0.9),
    clear = TRUE
  )
 
  # allowing progress bar to be used in foreach -----------------------------
  
  test <- mean(nchar(int_list))    # estimate the average token size and looks
  if (test >= 25) {                # whe the token is going to take up 
    progress <- function(n) {      # too much space
      pb$tick(tokens = list(item = n))    # report total items processed 
    }
  } else {
    progress <- function(n) {
      pb$tick(tokens = list(item = int_list[n]))     # report the int_list item 
    }
  }
  
  
  opts <- list(progress = progress)  # used in the the foreach loop 
  
  cat(crayon::style(crayon::bold('initializing foreach loop\n'), '#FF5500'))
  
  # foreach loop ------------------------------------------------------------
  if (is.null(dt)) {
    result <- foreach(
      i = 1:iterations,
      .combine = combine,
      .export = export,
      .options.snow = opts,
      .packages = PACKS
    ) %dopar% {
      FUN(int_list[i])
    }
    
  } else {
    result <- foreach(
      i = 1:iterations,
      .combine = combine,
      .options.snow = opts,
      .export = export,
      .packages = PACKS,
      .errorhandling = 'pass'
    ) %dopar% {
      FUN(dt = dt, int_list[i])
    }
    
  }
  
  cat(crayon::green('closing cluster\n'))
  
  if (return == TRUE) {
    result <- data.table::data.table(int_list, result)
    if (length(colnames(result)) == length(final.names)) {
      colnames(result) <- final.names
    } else {
      N <- length(final.names) - 1
      colnames(result)[c(dim(result)[2] - N:dim(result)[2])] <-
        final.names
    }
    return(result)
  } else if (return == 'simple') {
    return(result)
  } else {
    cat(crayon::green('FINISHED!'))
  }
}
