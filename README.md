
# GTEXseq

<!-- badges: start -->
[![Netlify Status](https://api.netlify.com/api/v1/badges/1ab9f310-e580-4c1e-9b46-6c2dd0240a05/deploy-status)](https://app.netlify.com/sites/eager-kirch-185a7e/deploys)

<!-- badges: end -->

The goal of GTEXseq is to allow rapid access to the gene expression data from the GTEX project. 

## Installation

You can install the released version of GTEXseq from [CRAN](https://CRAN.R-project.org) with:

``` r
remotes::install_gitlab("dbrookeUAB/GTEXseq")
```

## Example

This is a basic example which shows you how to solve a common problem:

``` r
library(GTEXseq)
## basic example code
```

